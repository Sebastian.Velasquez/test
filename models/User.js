'use strict'

const mongoose = require('mongoose')
const schema = mongoose.Schema

/*
 * Definition of an schema for the user model.
 * Will be used to make all the operations on a user
 */
const userSchema = schema(
    {
        id: {type: Number, required: true},
        name: String,
        last_name: String,
        address: String,
        city: String,
        state: String,
        country: String,
        phone: String,
        area_code: String,
        birthdate: Date
    },
    {
        versionKey: false
    }
)

module.exports = mongoose.model('User', userSchema)