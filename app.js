'use strict'

/*
 * This is the entry point of the application, here are initialized
 * the framework, the request parser, the router and the app itself
 */

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const router = require('./routes')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/api', router)

module.exports = app