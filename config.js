/*
 * Configuration file of the app.
 * If the environment variables are defined, the app will use it.
 * In other case it will use the default values.
 */

module.exports = {
    port: process.env.PORT || 3000,
    db: process.env.MONGODB_URI || 'mongodb://localhost:27017/users'
}