'use strict'

/*
 * Definition of the test for the app.
 * We are using mocha to run the test,
 * and chai to make the assertions
 */

process.env.NODE_ENV = "test"

let mongoose = require('mongoose')
let User = require("../models/User.js")

let chai = require('chai')
let chaiHttp = require('chai-http')
let should = chai.should()
chai.use(chaiHttp)

let app = require('../app')
const config = require('../config')

mongoose.connect(config.db, (err, res) => {
    if (err) throw err
    console.log('Conection to db successful')
    app.listen(config.port, () => {
        console.log(`Running in port ${config.port}`)
    })
})

beforeEach((done) => {
    User.remove({}, (err) => {
        done()
    })
})

describe('/GET api/users', () => {
    it('should return all the users', (done) => {
        chai.request(app)
            .get('/api/users')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('object')
                res.body.should.have.property('users')
                res.body.users.length.should.be.eql(0)
                done()
            })
    })
})

describe('/POST api/users', () => {
    it('should not save a user without an id', (done) => {
        let user = {
            name: "Sebastian",
            last_name: "Velasquez",
            address: "Calle 47n #3fn-10",
            city: "Cali",
            state: "Valle",
            country: "CO",
            phone: "3147065196",
            area_code: "57",
        }
        chai.request(app)
            .post('/api/users')
            .send(user)
            .end((err, res) => {
                res.should.have.status(400)
                res.body.should.be.an('object')
                res.body.should.have.property('message')
                done()
            })
    })
    
    it('should save a user', (done) => {
        let user = {
            id: 123456789,
            name: "Sebastian",
            last_name: "Velasquez",
            address: "Calle 47n #3fn-10",
            city: "Cali",
            state: "Valle",
            country: "CO",
            phone: "3147065196",
            area_code: "57",
        }
        chai.request(app)
            .post('/api/users')
            .send(user)
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.an('object')
                res.body.should.have.property('user')
                res.body.user.should.have.property('id')
                done()
            })
    })
})

describe('/GET api/users/:id', () => {
    it('should get a user by the given id', (done) => {
        let user = new User({
            id: 123456789,
            name: "Sebastian",
            last_name: "Velasquez",
            address: "Calle 47n #3fn-10",
            city: "Cali",
            state: "Valle",
            country: "CO",
            phone: "3147065196",
            area_code: "57",
        })
        user.save((err, res) => {
            chai.request(app)
                .get('/api/users/'+ user.id)
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('user')
                    res.body.user.should.have.property('id')
                    res.body.user.should.have.property('id').eql(user.id)
                    done()
                })
        })

    })

    it('should return 404 if the user searched by the given id doesnt exist', (done) => {
        chai.request(app)
            .get('/api/users/' + 123456789)
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.be.an('object')
                res.body.should.have.property('message')
                done()
            })
    })
})

describe('/PUT api/users/:id', () => {
    it('should update a user by a given id', (done) => {
        let user = new User({
            id: 123456789,
            name: "Sebastian",
            last_name: "Velasquez",
            address: "Calle 47n #3fn-10",
            city: "Cali",
            state: "Valle",
            country: "CO",
            phone: "3147065196",
            area_code: "57",
        })

        let userUpdate = new User({
            id: 123456789,
            name: "Juan",
            last_name: "Velasquez",
            address: "Calle 47n #3fn-10",
            city: "Cali",
            state: "Valle",
            country: "CO",
            phone: "3147065196",
            area_code: "57",
        })
        user.save((err, user) => {
            chai.request(app)
                .put('/api/users/' + user.id)
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object')
                    res.body.should.have.property('user')
                    res.body.user.should.have.property('name')
                    done()
                })
        })
    })
})

describe('/DELETE api/users/:id', () => {
    it('should delete a user by a given id', (done) => {
        let user = new User({
            id: 123456789,
            name: "Sebastian",
            last_name: "Velasquez",
            address: "Calle 47n #3fn-10",
            city: "Cali",
            state: "Valle",
            country: "CO",
            phone: "3147065196",
            area_code: "57",
        })
        user.save((err, user) => {
            chai.request(app)
                .delete('/api/users/' + user.id)
                .end((err, res) => {
                    res.should.have.status(204)
                    res.body.should.be.an('object')
                    done()
                })
        })
    })
})

